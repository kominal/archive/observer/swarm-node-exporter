import { AsyncClient, connect } from 'async-mqtt';
import { OBSERVER_MQTT_BROKER } from './helper/environment';
import { ContainersScheduler } from './scheduler/containers.scheduler';

export let mqttClient: AsyncClient;

export async function start() {
	mqttClient = connect(OBSERVER_MQTT_BROKER);
	await new ContainersScheduler().start(60);
}

start();
