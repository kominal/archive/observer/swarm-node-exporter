import { getEnvironmentString } from '@kominal/lib-node-environment';

export const OBSERVER_MQTT_BROKER = getEnvironmentString('OBSERVER_MQTT_BROKER');
export const OBSERVER_TENANT_ID = getEnvironmentString('OBSERVER_TENANT_ID');
export const PROJECT_NAME = getEnvironmentString('PROJECT_NAME');
export const ENVIRONMENT_NAME = getEnvironmentString('ENVIRONMENT_NAME');
export const NODE_HOSTNAME = getEnvironmentString('NODE_HOSTNAME');
